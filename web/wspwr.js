var sock = 0;
var sockUrl = "ws://localhost:8081/websocket";
var resetToken = 0;

var mhAuthResult = 0;
var mhSetPasswordResult = 0;
var mhDigitEvent = 0;

function AuthCtrl($scope, $location) {
    $scope.framework = "AngularJS";
    $scope.state = "NOT Authenticated";
    $scope.verificationDigits = 0;
    $scope.showDigits = false;
    $scope.enteredDigits = "";

    $scope.usePassword = function() {
        $scope.state = "Verifying Password ...";

        var authenticateMessage = {
            message: 'authenticate',
            body: {
                uidOrEmail: $scope.uidOrEmail,
                password: $scope.password
            }
        };

        sock.send(JSON.stringify(authenticateMessage));
    };

    $scope.usePhoneCall = function() {
        $scope.verificationDigits = Math.floor(Math.random() * 899999) + 100000;
        $scope.showDigits = true;
        $scope.state = "Placing verification phone call ...";

        var teleAuthMessage = {
            message: 'teleAuth',
            body: {
                uidOrEmail: $scope.uidOrEmail,
                digits: $scope.verificationDigits
            }
        }

        sock.send(JSON.stringify(teleAuthMessage));
    };

    $scope.mhAuthResult = function(body) {
        if (body.success) {
            resetToken = body.msg;
            $location.path('/setPassword');
        } else {
            $scope.state = "Password Verification Failed: " + body.msg;
        }

        $scope.$apply();
    }

    $scope.mhDigitEvent = function(body) {
        $scope.enteredDigits += body.digit;
        $scope.$apply();
    };

    mhAuthResult = $scope.mhAuthResult;
    mhDigitEvent = $scope.mhDigitEvent;
}

function SetPasswordCtrl($scope, $location) {
    $scope.state = "Authenticated";
    $scope.isComplete = false;

    $scope.setPassword = function() {
        $scope.state = "Setting new password ...";

        var setPasswordMessage = {
            message: 'setPassword',
            body: {
                resetToken: resetToken,
                password: $scope.password,
                confirmPassword: $scope.confirmPassword
            }
        };

        sock.send(JSON.stringify(setPasswordMessage));
    };

    $scope.mhSetPasswordResult = function(body) {
        if (body.success) {
            $scope.state = "Set Password Succeeded: " + body.msg;
            $scope.isComplete = true;
        } else {
            $scope.state = "Set Password Failed: " + body.msg;
            if (body.canRetry == false) $location.path('/auth');
        }

        $scope.$apply();
    };

    mhSetPasswordResult = $scope.mhSetPasswordResult;
}

angular.module('wspwr', []).config(['$routeProvider', function($routeProvider) {
    $routeProvider.
    when('/auth', { templateUrl: 'templ/auth.html', controller: AuthCtrl }).
    when('/setPassword', { templateUrl: 'templ/setPassword.html', controller: SetPasswordCtrl }).
    otherwise({ redirectTo: '/auth' });
}]);

$(document).ready(function() {
    // Open socket
    sock = new WebSocket(sockUrl);

    // Register open event handler
    sock.onopen = function() {
        $('#overlayConnecting').hide();
    };

    // Register message handlers
    sock.onmessage = function(evt) {
        console.log(evt.data);
        var msg = JSON.parse(evt.data);
        if (msg.message == 'authResult') { mhAuthResult(msg.body); }
        else if (msg.message == 'setPasswordResult') { mhSetPasswordResult(msg.body); }
        else if (msg.message == 'digitEvent') { mhDigitEvent(msg.body) }
    };
});

