package com.thogan.wspwr.telephony

import org.junit.{Assert, Test}
import org.junit.Assert._
import org.jboss.netty.buffer.ChannelBuffers
import org.jboss.netty.util.CharsetUtil

/**
 * Created with IntelliJ IDEA.
 * User: thogan
 * Date: 2/25/13
 * Time: 3:01 PM
 * To change this template use File | Settings | File Templates.
 */
class FreeswitchMessageDecoderTest {

    val testResponse =
        "Content-Type: api/response\n" +
        "Content-Length: 41\n" +
        "\n" +
        "+OK 29edc880-7f87-11e2-8111-11ce08db3c0e\n"

    val headersOnlyResponse =
        "Content-Type: command/reply\n" +
        "Reply-Text: +OK accepted\n\n"

    val incompleteResponse =
        "Content-Type: api/response\n" +
        "Content-L"

    @Test
    def testFindEndOfHeaders() {
        val decoder = new FreeswitchMessageDecoder
        val completeBuf = ChannelBuffers.copiedBuffer(testResponse, CharsetUtil.UTF_8)
        decoder.findEndOfHeaders(completeBuf) match {
            case Some(i) => assertTrue(i == 45); println("End of headers index: " + i)
            case None => fail("No index returned for buffer that should have had double newline")
        }

        val incompleteBuf = ChannelBuffers.copiedBuffer(incompleteResponse, CharsetUtil.UTF_8)
        decoder.findEndOfHeaders(incompleteBuf) match {
            case Some(i) => fail("Found end of headers in response that has no end of headers")
            case None =>
        }
    }

    @Test
    def testExtractHeaders() {
        val decoder = new FreeswitchMessageDecoder
        val buf = ChannelBuffers.copiedBuffer(testResponse, CharsetUtil.UTF_8)

        decoder.findEndOfHeaders(buf) match {
            case Some(eoh) => {
                val headers = decoder.extractHeaders(eoh)(buf)
                assertEquals(2, headers.size)
                headers foreach { p => println(p._1 + " --- " + p._2 )}
            }
            case None => fail("Could not find end of headers in test buffer")
        }
    }

    @Test
    def testDecode() {
        val decoder = new FreeswitchMessageDecoder
        var buf = ChannelBuffers.copiedBuffer(testResponse, CharsetUtil.UTF_8)

        decoder.decodeBuf(buf) match {
            case Some(msg) => {
                assertEquals(2, msg.headers.size)
                assertTrue(msg.data.length > 0)
                msg.headers foreach { p => println(p._1 + " --- " + p._2 )}
                println("DATA: " + msg.data)
            }
            case None => fail("Decode of response with data failed")
        }

        buf = ChannelBuffers.copiedBuffer(headersOnlyResponse, CharsetUtil.UTF_8)

        decoder.decodeBuf(buf) match {
            case Some(msg) => {
                assertEquals(2, msg.headers.size)
                assertTrue(msg.data.length == 0)
                msg.headers foreach { p => println(p._1 + " --- " + p._2 )}
                println("DATA: " + msg.data)
            }
            case None => fail("Decode of response without data failed")
        }
    }
}
