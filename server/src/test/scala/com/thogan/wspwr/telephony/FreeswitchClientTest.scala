package com.thogan.wspwr.telephony

import org.junit.{Ignore, Test}
import org.junit.Assert._

class FreeswitchClientTest {
    val host = "goltel.goliath.thogan.lan"
    val port = 8021
    val password = "tjh965"

    @Test
    def testAuth() {
        val client = new FreeswitchClient(host, port, password) {
            def onReady() {
                assertEquals(ClientState.Ready, state)
                close()
            }
        }

        client.connect()
        Thread.sleep(5000)
    }

    @Test
    @Ignore
    def testVerifyDigits() {
        val dest = "sofia/external/34378627#16127707166@sip.flowroute.com"
        val client = new FreeswitchVerifyDigits(host, port, password, dest, "123456", onComplete, onDigit)
        client.connect()
        Thread.sleep(30000)
        client.close()
    }

    def onComplete(v: Boolean): Unit = {
        println("Verfication " + (if (v) "Succeeded" else "Failed"))
    }

    def onDigit(digit: Char): Unit = {
        print(digit)
    }
}
