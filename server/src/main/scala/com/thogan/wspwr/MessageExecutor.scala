package com.thogan.wspwr

import messages.{TeleAuthMessageHandler, SetPassMessageHandler, AuthMessageHandler}
import net.liftweb.json._
import java.util.concurrent.Executors
import org.jboss.netty.channel.Channel
import org.jboss.netty.handler.codec.http.websocketx.TextWebSocketFrame

object MessageExecutor {
    val executor = Executors.newCachedThreadPool()
    def exec(h: WsPwrMessageHandler) = executor.execute(h)

    def routeAndExec(implicit ch: Channel, msg: String) = {
        val obj = parse(msg)
        implicit val body = obj \ "body"
        println("MESSAGE: " + (obj \ "message").values.toString)
        (obj \ "message") match {
            case JString("authenticate") => exec(new AuthMessageHandler().using)
            case JString("setPassword") => exec(new SetPassMessageHandler().using)
            case JString("teleAuth") => exec(new TeleAuthMessageHandler().using)
            case _ => println("Got message of unknown type: " + msg)
        }
    }
}

case class WsPwrMessage(message: String, body: Any)

abstract class WsPwrMessageHandler extends Runnable {
    implicit val formats = DefaultFormats

    var ch: Channel = _
    var body: JValue = _

    def run()

    def using(implicit ch: Channel, body: JValue) = {
        this.ch = ch
        this.body = body
        this
    }

    def sendResult(msg: WsPwrMessage) {
        val jsonMsg = Serialization.write(msg)
        ch.write(new TextWebSocketFrame(jsonMsg))
    }
}