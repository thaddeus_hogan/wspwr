package com.thogan.wspwr.telephony

import java.io.IOException
import collection.mutable.ArrayBuffer

class FreeswitchVerifyDigits(host: String, port: Int, password: String,
                             dest: String, digits: String, onComplete: (Boolean) => Unit,
                             onDigit: (Char) => Unit)
                             extends FreeswitchClient(host, port, password) {
    val verifyExt = "904"
    val verifyFailExt = "9041"
    val verifySuccessExt = "9042"

    val digitList = digits.toList
    val enteredDigits = new ArrayBuffer[Char]
    var currentDigit = 0

    def onReady() {
        originate(dest, verifyExt, true)
    }

    override def onOtherState(msg: FreeswitchMessage) {
        state match {
            case ClientState.ReceivingEvents => handleCallEventMessage(msg)
            case _ => throw new IOException("FreeswitchVerifyDigits client in bad state: " + state.toString)
        }
    }

    def handleCallEventMessage(msg: FreeswitchMessage) {
        msg.data split("\\n") filter(_.startsWith("DTMF-Digit")) headOption match {
            case Some(line) => handleDigit(line.split(": ", 2)(1).head)
            case None =>
        }
    }

    def handleDigit(digit: Char) {
        enteredDigits += digit
        onDigit(digit)


        if (currentDigit == (digitList.length - 1)) {
            if (enteredDigits == digitList) {
                transfer(verifySuccessExt)
                onComplete(true)
            } else {
                transfer(verifyFailExt)
                onComplete(false)
            }
        } else {
            if (enteredDigits(currentDigit) != digitList(currentDigit)) {
                transfer(verifyFailExt)
                onComplete(false)
            }
        }

        currentDigit += 1
    }
}
