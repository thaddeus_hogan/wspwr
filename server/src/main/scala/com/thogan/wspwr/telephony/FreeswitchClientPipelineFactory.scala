package com.thogan.wspwr.telephony

import org.jboss.netty.channel.{Channels, ChannelPipeline, ChannelPipelineFactory}
import org.jboss.netty.handler.codec.frame.DelimiterBasedFrameDecoder
import org.jboss.netty.buffer.ChannelBuffers
import org.jboss.netty.handler.codec.string.{StringEncoder, StringDecoder}

class FreeswitchClientPipelineFactory(client: FreeswitchClient) extends ChannelPipelineFactory {
    def getPipeline: ChannelPipeline = {
        val pipeline = Channels.pipeline()
        pipeline addLast ("framer", new FreeswitchMessageDecoder)
        pipeline addLast ("decoder", new StringDecoder)
        pipeline addLast ("encoder", new StringEncoder)
        pipeline addLast ("handler", new FreeswitchClientHandler(client))
        pipeline
    }
}
