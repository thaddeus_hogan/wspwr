package com.thogan.wspwr.telephony

case class FreeswitchMessage(headers: Map[String, String], data: String)
