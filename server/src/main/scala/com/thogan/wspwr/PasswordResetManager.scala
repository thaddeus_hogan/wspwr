package com.thogan.wspwr

import scala.collection.JavaConversions._
import java.util.concurrent.ConcurrentHashMap
import org.jboss.netty.channel.Channel
import java.util.Properties

/**
 * Created with IntelliJ IDEA.
 * User: thogan
 * Date: 2/23/13
 * Time: 5:07 PM
 * To change this template use File | Settings | File Templates.
 */

object PasswordResetManager {
    var config = new Properties

    def ldapHost = config.getProperty("ldapHost")
    def ldapPort = config.getProperty("ldapPort").toInt
    def ldapBase = config.getProperty("ldapBase")
    def resetterDn = config.getProperty("resetterDn")
    def resetterPassword = config.getProperty("resetterPassword")

    val resetTokens = new ExpiringConcurrentHashMap[String, String](5000)
}
